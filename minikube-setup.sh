#!/bin/bash

# Ubuntu 22.04

echo "Installing script tools..."
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y curl
sudo apt-get install -y iptables

# Mounting EBS volume
sudo mkfs -t xfs /dev/nvme1n1
sudo mkdir -p /data/usr/local/bin
sudo mount /dev/nvme1n1 /data
sudo cp /etc/fstab /etc/fstab.orig
sudo echo "export PATH='/data/usr/local/bin/:$PATH'" >> .profile

if ! [ -x "$(command -v kubectl)" ]; then
    echo "Downloading Kubectl binaries..."
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
    echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
    if [ $? == 0 ]; then
        echo "Installing Kubectl..."
        sudo install -o root -g root -m 0755 kubectl /data/usr/local/bin/kubectl
        if ! [ -x "$(command -v minikube)" ]; then
            echo "Installing Minikube..."
            curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
            sudo install minikube-linux-amd64 /data/usr/local/bin/minikube
        else
            echo "Minikube is already installed"
        fi

        echo "Installing Docker..."
        curl -fsSL https://get.docker.com -o get-docker.sh
        sudo sh get-docker.sh

        # Add ubuntu user to Docker group:
        sudo usermod -aG docker $USER && newgrp docker

        # Installing --vm-driver=none dependencies
        sudo apt-get install -y conntrack

        sudo wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.26.0/crictl-v1.26.0-linux-amd64.tar.gz
        sudo tar zxvf crictl-v1.26.0-linux-amd64.tar.gz -C /usr/local/bin
        sudo mkdir /usr/local/bin/cri-dockerd
        sudo wget https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.4/cri-dockerd_0.3.4.3-0.ubuntu-jammy_amd64.deb
        sudo dpkg -i cri-dockerd_0.3.4.3-0.ubuntu-jammy_amd64.deb

        # CNI plugins
        cd /data
        sudo curl -LO https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz
        CNI_PLUGIN_INSTALL_DIR="/opt/cni/bin"
        sudo mkdir -p "$CNI_PLUGIN_INSTALL_DIR"
        sudo tar -xf "cni-plugins-linux-amd64-v1.3.0.tgz" -C "$CNI_PLUGIN_INSTALL_DIR"

        echo "Starting minikube..."
        minikube start --vm-driver=none
        minikube addons enable ingress

    fi
fi