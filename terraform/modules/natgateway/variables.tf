variable "vpc_id" {
  type = string
}

variable "nat_subnet" {
  type = string
}

variable "project_name" {
  type        = string
  description = "Define Project Name"
}

variable "env" {
  type = string
}

variable "tags" {
  type = map(string)
}

