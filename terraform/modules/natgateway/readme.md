# Módulo Nat Gateway
Este módulo é responsável por criar um NAT Gateway para fornecer conectividade à Internet para as sub-redes privadas em uma VPC.

## Uso
Exemplo de utilização do módulo:
```hcl
module "nat" {
  depends_on   = [module.vpc, module.public_subnets]
  source       = "../../modules/natgateway"
  nat_subnet   = module.public_subnets["pub-sb-eks1-a"].id
  vpc_id       = module.vpc.id
  project_name = var.project_name
  env          = var.env
  tags         = var.tags
}
```

## Entradas

| Nome          | Tipo         | Descrição                                  |
|---------------|--------------|--------------------------------------------|
| vpc_id        | string       | ID da VPC                                  |
| nat_subnet    | string       | ID da sub-rede em que o NAT Gateway será criado |
| project_name  | string       | Nome do projeto                            |
| env           | string       | Ambiente                                   |
| tags          | map(string)  | Tags adicionais para os recursos criados   |

## Saídas

| Nome  | Descrição                         |
|-------|-----------------------------------|
| id    | ID do NAT Gateway                 |

## Recursos Criados

- `Elastic IP`: Cria um Elastic IP para o NAT Gateway.
- `Nat Gateway`: Cria um NAT Gateway para fornecer conectividade à Internet para as sub-redes privadas.