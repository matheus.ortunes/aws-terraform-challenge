resource "aws_eip" "nat_eip" {
  domain = "vpc"
  tags = merge(
    { Name = "eip-${var.project_name}-${var.env}" },
    var.tags
  )
}

# NAT Gateway Private - for the private subnets 
resource "aws_nat_gateway" "nat" {
  connectivity_type = "public"
  allocation_id     = aws_eip.nat_eip.id
  subnet_id         = var.nat_subnet
  tags = merge(
    { Name = "nat-${var.project_name}-${var.env}" },
    var.tags
  )
}