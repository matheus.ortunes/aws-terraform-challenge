variable "vpc_id" {
  type        = string
  description = "Define VPC CIDR block"
}

variable "rt_association" {
  type = list(string)
}

variable "ig_id" {
  type = string
}

variable "name" {
  type = string
}

variable "nat_id" {
  type = string
}

variable "use_nat_gateway" {
  type = string
}

variable "tags" {
  type = map(string)
}
