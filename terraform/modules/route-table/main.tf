# Public subnets route table
resource "aws_route_table" "main" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    //gateway_id = var.ig_id
    # Condição para usar nat_gateway_id ou internet_gateway_id
    nat_gateway_id = var.use_nat_gateway ? var.nat_id : null
    gateway_id     = var.use_nat_gateway ? null : var.ig_id
  }

  tags = merge(
    { Name = "${var.name}" },
    var.tags
  )
}

# Associate the public route table to public subnets
resource "aws_route_table_association" "public" {
  count          = length(var.rt_association)
  subnet_id      = var.rt_association[count.index]
  route_table_id = aws_route_table.main.id
}

