# Módulo Route Table
Este módulo é responsável por criar tabelas de roteamento para sub-redes públicas ou privadas em uma VPC.

## Uso
```hcl
module "rt-public" {
  depends_on      = [module.vpc]
  source          = "../../modules/route-table"
  vpc_id          = module.vpc.id
  ig_id           = module.vpc.ig-id
  tags            = merge({ Name = "rt-${var.project_name}-${var.env}-public" }, var.tags)
  use_nat_gateway = false
  nat_id          = null
  rt_association  = flatten([for subnet in values(module.public_subnets) : subnet.id])
}

module "rt-private" {
  depends_on      = [module.vpc, module.nat]
  source          = "../../modules/route-table"
  vpc_id          = module.vpc.id
  nat_id          = module.nat.id
  tags            = merge({ Name = "rt-${var.project_name}-${var.env}-private" }, var.tags)
  use_nat_gateway = true
  ig_id           = null
  rt_association  = flatten([for subnet in values(module.private_subnets) : subnet.id])
}
```

## Inputs
| Nome              | Tipo         | Descrição                                   |
|-------------------|--------------|---------------------------------------------|
| vpc_id            | string       | ID da VPC                                   |
| rt_association    | list(string) | Lista de IDs das sub-redes a serem associadas à tabela de roteamento |
| ig_id             | string       | ID do Internet Gateway para roteamento público (opcional)  |
| nat_id            | string       | ID do NAT Gateway para roteamento privado (opcional)     |
| use_nat_gateway   | string       | Indica se deve ser usado NAT Gateway para roteamento privado (true/false) |
| tags              | map(string)  | Tags adicionais para os recursos criados      |

## Outputs
| Nome              | Tipo         | Descrição                                   |
|-------------------|--------------|---------------------------------------------|
| id                | string       | ID da Route table criada                             |
| arn               | string       | Arn da Route table  criada |

## Recursos Criados
- `Route table`: Cria uma tabela de roteamento para a VPC especificada.
- `Route table association`: Associa a tabela de roteamento pública ou privada às sub-redes especificadas.

## Notas
- Para sub-redes públicas deve ser referenciado a variavél use_nat_gateway como false e passado por parâmetro o internet gateway id.
- Para sub-redes privadas deve ser referenciado a variável use_nat_gateway como true e passado por parâmetro o nat gateway id.