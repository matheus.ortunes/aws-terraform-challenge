#--------------------------------------------
# Deploy EC2
# Documentation: https://registry.terraform.io/modules/terraform-aws-modules/ec2-instance/aws/5.5.0
#--------------------------------------------

module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name = "b2b-k8s-instance"

  instance_type               = "t3.medium"
  vpc_security_group_ids      = [aws_security_group.b2b-k8s-sg.id]
  subnet_id                   = module.private_subnets["private-subnet-a"].id
  associate_public_ip_address = true
  key_name                    = data.aws_key_pair.aws-terraform-challenge.key_name
  ami   = "ami-0af6e9042ea5a4e3e"
  user_data = file("../../../minikube-setup.sh")

  tags = var.tags
}

resource "aws_instance" "bastion_host" {
  ami           = "ami-08f03e8afb312b97a"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.b2b-k8s-sg.id]
  subnet_id                   = module.public_subnets["public-subnet-a"].id
  key_name                    = data.aws_key_pair.aws-terraform-challenge.key_name
  instance_type               = "t3.nano"

  tags = var.tags
}

resource "aws_instance" "gitlab_runner" {
  ami           = "ami-08f03e8afb312b97a"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.b2b-k8s-sg.id]
  subnet_id                   = module.public_subnets["public-subnet-a"].id
  key_name                    = data.aws_key_pair.aws-terraform-challenge.key_name
  instance_type               = "t3.nano"

  tags = var.tags
}

resource "aws_volume_attachment" "b2b-k8s-ebs" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.b2b-k8s-ebs.id
  instance_id = module.ec2_instance.id
}

resource "aws_ebs_volume" "b2b-k8s-ebs" {
  availability_zone = "sa-east-1a"
  size              = 25

  tags = var.tags
}

# #--------------------------------------------
# # Deploy ALB
# # Documentation: https://registry.terraform.io/modules/terraform-aws-modules/alb/aws/8.7.0
# #--------------------------------------------

# module "alb_ecs" {
#   source  = "terraform-aws-modules/alb/aws"
#   version = "8.7.0"

#   name     = "alb-internal-${var.project_name}-${var.env}"
#   internal = true

#   load_balancer_type = "application"

#   vpc_id  = module.vpc.id
#   subnets = module.private_subnets["private-subnet-a"].id
#   #subnets = module.public_subnets["public-subnet-a"].id
#   security_groups = ["${aws_security_group.alb-sg.id}"]

# to do
#   access_logs = {
#     bucket = "alb-logs"
#   }

#   target_groups = [
#     {
#       name_prefix      = "b2b-"
#       backend_protocol = "HTTP"
#       backend_port     = 80
#       target_type      = "ip"
#       health_check = {
#         enabled             = true
#         interval            = 30
#         path                = "/healthcheck"
#         port                = "traffic-port"
#         healthy_threshold   = 5
#         unhealthy_threshold = 2
#         timeout             = 6
#         protocol            = "HTTP"
#         matcher             = "200"
#       }
#     }
#   ]

#   http_tcp_listeners = [
#     {
#       port               = 80
#       protocol           = "HTTP"
#       target_group_index = 0
#     }
#   ]

#   # https_listeners = [
#   #   {
#   #     port               = 80
#   #     protocol           = "HTTPS"
#   #     target_group_index = 0
#   #   }

#   # ]

#   tags = var.tags
# }
