#-------------------------------------------------------------
# Deploy S3 bucket to keep terraform state. Run only-once
#-------------------------------------------------------------

module "terraform_backend_state" {
  source = "../../../modules/remote-state"
  name   = "b2b-k8s-remote-state"

  s3_versioning = true

  tags = {
    "project"   = "aws-terraform-challenge"
    "terraform" = "true"
  }
}