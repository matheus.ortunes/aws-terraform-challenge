terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.57"
    }
  }
}

provider "aws" {
  profile = "tf-user"
  region  = "sa-east-1"
}