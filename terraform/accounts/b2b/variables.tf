variable "tags" {
  type = map(string)

  default = {
    "project"   = "aws-terraform-challenge"
    "terraform" = "true"
    "env"       = "challenge"
  }
}

variable "region" {
  type    = string
  default = "sa-east-1"
}

variable "profile" {
  type    = string
  default = "tf-user"
}

variable "project_name" {
  type    = string
  default = "aws-terraform-challenge"
}

variable "env" {
  type    = string
  default = "devops"
}

#--------------------------------------------
# Variables to deploy VPC and Subnet modules
#--------------------------------------------
variable "availability_zones" {
  type = map(string)
  default = {
    a = "sa-east-1a"
    b = "sa-east-1b"
    c = "sa-east-1c"
  }
  description = "Define subnets AZs"
}

variable "vpc_cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "Define VPC CIDR block"
}

variable "public_subnets" {
  type = map(object({
    name              = string
    cidr_block        = string
    availability_zone = string
  }))
  default = {
    "public-subnet-a" = {
      name              = "public-subnet-challenge-a"
      cidr_block        = "10.0.0.0/24"
      availability_zone = "sa-east-1a"
    }
  }
}

variable "private_subnets" {
  type = map(object({
    name              = string
    cidr_block        = string
    availability_zone = string
  }))
  default = {
    "private-subnet-a" = {
      name              = "private-subnet-challenge-a"
      cidr_block        = "10.0.1.0/24"
      availability_zone = "sa-east-1a"
    }
  }
}