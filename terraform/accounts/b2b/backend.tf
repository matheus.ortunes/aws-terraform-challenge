terraform {
  backend "s3" {
    bucket  = "b2b-k8s-remote-state"
    key     = "tf.tfstate"
    region  = "sa-east-1"
    profile = "tf-user"
  }
}